package org.example.university_lab_4.matrixFilling

import kotlin.math.pow
import kotlin.properties.Delegates

/**
 * Builder for matrix
 */
class MatrixBuilder {
    private var counter: Int = 1
    private var size by Delegates.notNull<Int>()
    private var branchNumber = 0
    private var row = 0
    private var column = 0
    private var i = 0
    private var j = 0
    private var sum = 0

    /**
     * Build body with length equal size, and calculate first sum of an arithmetic progression
     * @param size size of matrix
     */
    fun buildSize(size: Int) {
        this.size = size
        sum = 2 * size - 1
    }
    /**
     * Allocates a matrix cell based on the number of the current element and writes value there
     * @param matrix where we allocate cell
     */
    fun buildCell(matrix: Matrix) {
        if (counter > sum) {
            branchNumber++
            sum += 2 * size - 2 * (branchNumber) - 1
        }

        j = size - branchNumber - 1
        i = (counter - size + (-1).toDouble().pow(branchNumber + 1).toInt() * j - 2 * branchNumber * size + branchNumber * (branchNumber + 1)) * (-1).toDouble().pow(branchNumber + 1).toInt()

        if (size - branchNumber - 1 >= i) {
            row = i
            column = size - j - 1
        } else {
            row = j
            column = size - (counter - size + (-1).toDouble().pow(branchNumber).toInt() * j - 2 * branchNumber * size + branchNumber * (branchNumber + 1)) * (-1).toDouble().pow(branchNumber).toInt() - 1
        }

        matrix.set(row, column, counter)
        counter++
    }
}
