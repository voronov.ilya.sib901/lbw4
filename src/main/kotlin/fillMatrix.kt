package org.example.university_lab_4.matrixFilling

import java.lang.IllegalArgumentException

/**
 * Matrix filling according rule.
 * Changes matrix example of changed matrix for dim 4x4:
 * @param matrix - quad matrix of Ints
 */
fun fillMatrix(matrix: Matrix, size: Int) {
    if (size <= 0) {
        throw IllegalArgumentException("Wrong dimension: it should be greater than 0")
    }
    val resultBuilder = MatrixBuilder()
    resultBuilder.buildSize(size)
    (1 until size * size + 1).forEach() {
        resultBuilder.buildCell(matrix)
    }
}
