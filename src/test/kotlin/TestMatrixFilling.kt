package org.example.university_lab_4.matrixFilling

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

class TestMatrixFilling {

    @Test
    fun `Size 0`() {
        var matrix = mock(Matrix::class.java)
        try {
            fillMatrix(matrix, 0)
        } catch (ex: IllegalArgumentException) {
            assert(ex.message == "Wrong dimension: it should be greater than 0")
        }
    }

    @Test
    fun `Size lower than 0`() {
        var matrix = mock(Matrix::class.java)
        try {
            fillMatrix(matrix, -1)
        } catch (ex: IllegalArgumentException) {
            assert(ex.message == "Wrong dimension: it should be greater than 0")
        }
    }

    @Test
    fun `Matrix 1 x 1`() {
        var m = mock(Matrix::class.java)

        fillMatrix(m, 1)

        Mockito.verify(m).set(0, 0, 1)
    }

    @Test
    fun `Matrix 3 x 3`() {
        var matrix = mock(Matrix::class.java)

        fillMatrix(matrix, 3)

        Mockito.verify(matrix).set(0, 0, 5)
        Mockito.verify(matrix).set(0, 1, 6)
        Mockito.verify(matrix).set(0, 2, 9)

        Mockito.verify(matrix).set(1, 0, 4)
        Mockito.verify(matrix).set(1, 1, 7)
        Mockito.verify(matrix).set(1, 2, 8)

        Mockito.verify(matrix).set(2, 0, 3)
        Mockito.verify(matrix).set(2, 1, 2)
        Mockito.verify(matrix).set(2, 2, 1)
    }

    @Test
    fun `Matrix 4 x 4`() {
        var matrix = mock(Matrix::class.java)

        fillMatrix(matrix, 4)

        Mockito.verify(matrix).set(0, 0, 7)
        Mockito.verify(matrix).set(0, 1, 8)
        Mockito.verify(matrix).set(0, 2, 15)
        Mockito.verify(matrix).set(0, 3, 16)

        Mockito.verify(matrix).set(1, 0, 6)
        Mockito.verify(matrix).set(1, 1, 9)
        Mockito.verify(matrix).set(1, 2, 14)
        Mockito.verify(matrix).set(1, 3, 13)

        Mockito.verify(matrix).set(2, 0, 5)
        Mockito.verify(matrix).set(2, 1, 10)
        Mockito.verify(matrix).set(2, 2, 11)
        Mockito.verify(matrix).set(2, 3, 12)

        Mockito.verify(matrix).set(3, 0, 4)
        Mockito.verify(matrix).set(3, 1, 3)
        Mockito.verify(matrix).set(3, 2, 2)
        Mockito.verify(matrix).set(3, 3, 1)
    }
}
